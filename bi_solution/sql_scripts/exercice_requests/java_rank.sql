SELECT * FROM
(   
    SELECT avg_prom, island,
    DENSE_RANK () OVER (
        ORDER BY avg_prom DESC
    ) grade_rank
    FROM (
        SELECT AVG(prom) AS avg_prom, island
        FROM "public"."300_km_isolated" 
        GROUP BY island
        
    ) average
    WHERE island IS NOT null
) ranks

WHERE ranks.island='Java'

