SELECT t.peak, t.continent, RANK () OVER (ORDER BY elev DESC) second_peak_rank FROM (
    SELECT *, RANK () OVER (
        PARTITION BY continent
        ORDER BY elev DESC
    ) AS continent_rank
    FROM "public"."300_km_isolated" AS peak 
    JOIN "public"."range" ON "public"."range".range = peak.range
) t
WHERE t.continent_rank=2
