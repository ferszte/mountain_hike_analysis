WITH averages AS (
    SELECT * FROM (
        SELECT country, AVG(prom) AS average_prom, AVG(isol) AS average_isol 
        FROM "public"."300_km_isolated" 
        GROUP BY country
    ) t 
)
SELECT * FROM averages WHERE average_prom=(SELECT MAX(average_prom) FROM averages)

