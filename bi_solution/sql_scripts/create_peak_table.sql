CREATE TABLE "300km_isolated"
(
    peak varchar(50),
    elev integer NULL,
    prom integer NULL,
    isol double precision NULL,
    country varchar(50),
    range varchar(50),
    island varchar(50)
)
