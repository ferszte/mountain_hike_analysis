import numpy as np
import pandas as pd
from typing import Any


def run_processing() -> None:
    peak_csv_path = "../data/300_km_isolated.csv"
    
    df_peaks = pd.read_csv(peak_csv_path)

    col_names_mapping = {"Elev-M": "Elev", "Prom-M": "Prom", "Isol-Km": "Isol"}
    col_types_mapping = {
        "Elev": 'Int64',
        "Prom": 'Int64',
        "Isol": float
    }

    processed_df = rename_df_columns(
        replace_non_numerical_values(df_peaks, "Isol-Km"),
        col_names_mapping
    )

    typed_df = set_df_types(processed_df, col_types_mapping)

    typed_df.to_csv("processed_300_km_isolated.csv", sep=",", index=False)


def replace_non_numerical_values(dataframe: pd.DataFrame, col_name: str, replace_value: Any = np.nan) -> pd.DataFrame:
    dataframe[col_name] = pd.to_numeric(dataframe[col_name], errors='coerce')

    if replace_value != np.nan:
        return dataframe.fillna(replace_value)
    else:
        return dataframe


def set_df_types(dataframe: pd.DataFrame, col_types_mapping: dict) -> pd.DataFrame:
    return dataframe.astype(col_types_mapping)


def rename_df_columns(dataframe: pd.DataFrame, columns_mapping: dict) -> pd.DataFrame:
    return dataframe.rename(columns_mapping, axis=1)


if __name__ == '__main__':
    run_processing()
