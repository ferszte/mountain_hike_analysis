
# Moutain Hiking Analysis

## Aim of the exercice 
Provide a datasets containing information to help prepare for mountain hiking.


## Questions
1. What is the rank of Java in the island ranking based on average prominence ?
    
2. What is the average isolation of the country which has the biggest average prominence ?
    
3. What is continent ranking based on the second highest peak ?
    
4. Which peak is the farthest from the center of earth ?

> 4. As we suppose the earth to be totally spheric, the farthest peak from the center of the earth corresponds to the higher (the one with the biggest elevation)


## Proposal

### Functional answer 

Before digging deeper we need to answer the following questions : 
* As an end-user point of view, what are the criterias that will make me choose a destination for hiking, depending on my level?
* What kind of data do we need to answer to the problem ? 
* What is the content of the input files ?
* Is it enough to answer to the problem  ?
* Which specific set of this data will help us to answer to the problem ? 


#### Answers

`As an end-user point of view, what are the criterias that will make me choose a destination for hiking, depending on my level?`

Following criterias seems to be relevant in a moutanin hiking plan : 
* The location
* The elevation
* The prominence
* The isolation
* The peak name (is the peak famous?)

Basically, all those criteria refere to the difficulty of the hike (and the destination...). Then, we can add one criteria : Does the peak is an ultra-prominent peak, also called a **P1500** (for the hard hikers)

> An ultra-prominent peak, also called a **P1500**, is a mountain summit with a topographic prominence of 1,500 metres


`What kind of data do we need to answer to the problem?`
Regarding the four questions of the exercices, and globally, the context : we need informations about the size of the peak, its location, and about the difficulty of the hike (elevation/prominence/isolation).

`What is the content of the input files?`
1. **300_km_isolated.csv** contains technical informations about all peaks (listed on peak bagger) 
2. **range.csv** contains a list of mountain range and their corresponding continent (listed on peak bagger)
3. **mountains.json**  contains a list of mountains with geo-localization information

`Is it enough to answer to the problem?`
Yes, we have enough informations to sort out all the hike destination depending on the target level, from almost flat hike to hardcore level.

`Which specific set of this data will help us to answer to the problem ?` 

To answer to the problem, as already mentioned, we need technical informations about all peaks. the first file :  **300_km_isolated.csv** gave us all the technical informations we need, and **range.csv** gave us more precision on the location (the continent). However, the third file : **mountain.json** will not be useful, as it only contains some basic properties on each mount (but not enough precise), and some very precise location informations, which doesn't seem to be needed to correctly answer to the problematic.

> It could be useful if we want to visualise, precisely on a map, the location of each mountain. In such a case, as the source is different from other files, some data processing would be necessary to make it exploitable : ensure the name corresponds etc... 

### Data glossary 

Following, the data glossary of the used files : 

* `300_km_isolated.csv`
> This file lists the mountains with isolation higher than 300 kms.
> Source : https://www.peakbagger.com

| Field Name | Expected Data Type | Meaning                                                                                                                                                                    | Observation                                     |   |
|------------|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------|---|
| Peak       | String             | Name of the peak                                                                                                                                                           |                                                 |   |
| Elev-M     | Integer            | The elevation of a geographic location is its height above or below a fixed reference point. Metric : Meter                                                                |                                                 |   |
| Prom-M     | Integer            | Prominence measures the height of a mountain or hill's summit  relative to the lowest contour line encircling it but containing  no higher summit within it. Metric: Meter |                                                 |   |
| Isol-km    | Double             | Isolation of a summit is the minimum distance to a point of equal  elevation, representing a radius of dominance in which the peak is  the highest point                   |                                                 |   |
| Country    | String             | Name of the country / countries the peak belong to                                                                                                                         | Can contains multiple country                   |   |
| Range      | String             | Name of the mountains range the peak belong to                                                                                                                             |                                                 |   |
| Island     | String             | Name of the island on which the peak is located.                                                                                                                           | Only exists when a peak is located on an island |   |

* `range.csv`
> Source : https://www.peakbagger.com

| Field Name | Expected Data Type | Meaning                                            | Observation |
|------------|--------------------|----------------------------------------------------|-------------|
| Range      | String             | Name of the mountains range                        | None        |
| Continent  | String             | Name of the continent the mountain range belong to | None        |


> NB : The **Range** field can be used as a join key to retrieve the continent information.


### Technical Answer

#### The basic one
As the volume of data is really small and the used files contains tabulate data, many technologies can be suitable to technically answer to this problem.  A combination of python and pandas framework (in Jupiter notebook) is a great way to bring a quick, efficient, and simple answer.

Pros and cons:  
This is a good way to explore the data and have visualise it, it's a good first step but clearly not a solution suitable in a long term view 

##### Execution
First create a virtual environment 
`python3 -m venv critizr-env`

Activate it 
`source critizr-env/bin/activate`

Install the dependencies
`pip install -r requirements.txt`

The notebook *moutain_hike_analysis* can now be run.

#### The more "production" ready one 

We can bring this analysis to the next level, using a BI tool, which will allow us to make some interactives queries (for exemple, getting information only for a specific country ...).

For this purpose, we can use "Metabase" an open source BI tool. One of the main requirement will be to have our data in an sql database.

For simplicity, both Metabase and the selected database (PostgresSQL) will be setup using docker-compose.

##### Steps 
Following are the technical steps we need to complete : 
* Setup PostgresSQL and Metabase:
`docker-compose up -d`
* Create the table for range and peak data : (use sql scripts create_table_*.sql)
* Pre-process the data to be compliant with the table DDL: `python preprocessing.py` - Should be done with requirements installed
* Inject csv data inside the table
* Develop the sql requests (analytics)
* Organize the dashboard based on the requests result

> The postgres instance living in a docker container, i use pg-admin (as a shortcut...) to easily uploaded csv data into the previously created table.
This avoid to run a sql script at the container creation etc...

Following a screenshot of the developped dashboard (will be presented in live during the next interview session):

<p align="center"><img src="assets/metabase_view.png" height=700/></p>
<br/>

> In addition to the main kpi corresponding to the 4 questions, we can have some other graph easily and link them with some filter. Here for exemple, we've a "Country" filter, which allow us to pick a country, and have the data updated, based on its value. The widget not link with the filter will not have their data updated

Result based on "China" value:

<br/>
<p align="center"><img src="assets/metabase_view2.png" height=700/></p>


## Global Answer 

1. Java Island rank (based on average prominence) is 6.
2. The average isolation of the country which has the biggest average prominence is null (don't have any prominence value for China/Nepal), for the second one, the average prominence is 2812
3. For continent ranking, based on second highest peak : see table in screenshot
4. Peak which is the farthest from the center of eart: Mount everest
